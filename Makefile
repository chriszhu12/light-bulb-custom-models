all: keras_language_model.tar.gz cat_not_cat.tar.gz small_imdb_reviews.tar.gz json_classification.tar.gz

.PHONY: clean

keras_language_model.tar.gz:
	tar -zcf keras_language_model.tar.gz keras_language_model

cat_not_cat.tar.gz:
	tar -zcf cat_not_cat.tar.gz cat_not_cat

json_classification.tar.gz:
	tar -zcf json_classification.tar.gz json_classification

small_imdb_reviews.tar.gz:
	tar -zcf small_imdb_reviews.tar.gz small_imdb_reviews

clean:
	rm keras_language_model.tar.gz cat_not_cat.tar.gz small_imdb_reviews.tar.gz json_classification.tar.gz json_classification.tar.gz
